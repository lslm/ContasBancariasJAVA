# EXERCÍCIO DE POLIMORFISMO
Um banco tem 3 (três) tipos de conta: corrente, poupança e universitária.
Todas os 3 tipos possuem alguns dados em comum: agência, número e saldo no início do mês (na prática, não se usa double para saldo e sim BigDecimal ou algo parecido). Uma agência tem um nome e um endereço (geralmente definimos endereço como uma classe separada). Uma conta é de um cliente, que tem um nome e uma data de nascimento.

A seguir, as peculiaridades de cada conta são descritas (o cenário é fictício):

* Conta corrente: cada saque e retirada de extrato implica em uma taxa de 10 centavos de reais. Saques maiores que 5.000 implicam em uma taxa de 1% sobre o saque.
* Conta poupança: não possui taxas. Deve ser possível calcular o rendimento da poupança para um determinado valor de saldo (0,5%).
* Conta universitária: também não possui taxas. Cada vez que o cliente movimenta a conta (saque ou depósito), ele ganha 10 centavos de bônus. Ainda, contas universitárias podem fazer empréstimos bancários de até 2.000. O valor do empréstimo é disponibilizado em conta quando ele feito. Uma conta pode fazer apenas 1 empréstimo.

Implemente um aplicativo de banco que permite o cadastro de agências, contas, a realização de operações bancárias básicas (extrato, depósito e saque). Ainda, o aplicativo deve calcular o saldo total do banco. Por fim, ele deve exibir uma lista das contas ordenadas pelo nome do cliente.

###### Profº Leandro Luque