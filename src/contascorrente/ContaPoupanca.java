package contascorrente;

import java.math.BigDecimal;

/**
 *
 * @author Lucas Santos
 */
public class ContaPoupanca extends Conta {
    /**
     * @description Metodo que retorna o rendimento do saldo atual (0.5%)
     * @return saldo * 1.005
     */
    public BigDecimal getRendimento() {
        return this.getSaldo().add(new BigDecimal(1.005));
    }
}
